//
//  ResultadoPartidoTableViewCell.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 12/6/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class ResultadoPartidoTableViewCell: UITableViewCell {

    @IBOutlet weak var ViewOfCell: UIView!
    @IBOutlet weak var lbNameLocalTeam: UILabel!
    @IBOutlet weak var lbNameVisitTeam: UILabel!
    @IBOutlet weak var lbResultLocalTeam: UILabel!
    @IBOutlet weak var lbResultTeamVisit: UILabel!
    @IBOutlet weak var lbFechaMatch: UILabel!
    
    private var partido : PartidoStruct!  {
        didSet{
            self.updateCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell() -> Void {
        
        ViewOfCell.backgroundColor = UIColor.white
        contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
        ViewOfCell.layer.cornerRadius = 10.0
        ViewOfCell.layer.masksToBounds = false
        ViewOfCell.layer.shadowColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2).cgColor
        ViewOfCell.layer.shadowOffset = CGSize(width: 0, height: 0)
        ViewOfCell.layer.shadowOpacity = 0.8
        
        
    }

}
