//
//  CoreDataStack.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 12/2/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    private let coreDataModelName : String
    
    init(dataModelName:String) {
        self.coreDataModelName = dataModelName
    }
    
    private lazy var storeContainer : NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: self.coreDataModelName)
        
        container.loadPersistentStores {
            (storesDescription, error) in
            
            if let error = error as NSError? {
                print("ocurrio un error, \(error.localizedDescription)")
            }
        }
        
        return container

        
    }()

    lazy var contextObject : NSManagedObjectContext = {
       
        let objectContext = self.storeContainer.viewContext
        return objectContext
        
    }()
    
    
    func sabeContext(){
        
        guard self.contextObject.hasChanges else{
            print("no hay cambios por guardar")
            return
        }
        
        do {
            
            try self.contextObject.save()
            
        }catch let error as NSError{
            print("Hubo un error al gaurdar los datos \(error)")
        }
        
        
    }
    
    
    
    
}
