//
//  RegisterPresenter.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import CoreData

class RegisterPresenter: RegisterViewPresenter {

    
    private let registerView:RegisterView
    private var _manageContext : NSManagedObjectContext
    var userDataService : UserDataService
    
    required init(view:RegisterView, manageContext:NSManagedObjectContext) {
        self.registerView = view
        self._manageContext = manageContext
        self.userDataService = UserDataService(manageContext: self._manageContext)
        
    }
    
    
    func registrarUsuario(usuario:UsuarioStruct) {
        
        do{
            try userDataService.RegisterUser(user: usuario)
            
        }catch let error as NSError{
            print("error al guardar el usuario")
        }
        
        
    }
    
    
    
}
