//
//  LoginPresenter.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import  CoreData

class LoginPresenter: LoginViewPresenter {
    private let loginView: LoginView
    private let manageContext : NSManagedObjectContext
    private let LoginDataService: UserDataService
    
    required init(view:LoginView, _manageContex:NSManagedObjectContext) {
        self.loginView = view
        self.manageContext = _manageContex
        self.LoginDataService = UserDataService(manageContext: self.manageContext)
        
    }
    
    func LoginUsuario(email: String, contrasena: String) -> UsuarioStruct {
        
        do{
            
        var usuarioLogin = try LoginDataService.Login(email: email, password: contrasena)
        return usuarioLogin
        
        }catch let error as NSError{
            print("Ocurrio un error en el login \(error)")
        }
    }
    
}
