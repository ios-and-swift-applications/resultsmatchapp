//
//  RegisterResultMatchPresenter.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 12/6/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import CoreData

class RegisterResultMatchPresenter : RegisterResultMatchViewPresenter {
  
    
    
    private var  manageContext : NSManagedObjectContext
    private var resultadoDataService : ResultadosDataService
    private var registerResultMatchView : RegisterResultMatchView
    
    required init(view: RegisterResultMatchView, manageContext: NSManagedObjectContext) {
        self.registerResultMatchView = view
        self.manageContext = manageContext
        self.resultadoDataService = ResultadosDataService(_manageContext: self.manageContext)
        
    }
    
    func RegisterResultMatch(partido: PartidoStruct) {
    
        do{
             resultadoDataService.SaveResultMatch(partido: partido)
            
        }catch let error as NSError{
            print("Ocurrio un error")
        }
        
    }
    
    func GetAllResultMatches() -> [PartidoStruct] {
        
        var resultMatches : [PartidoStruct] = []
        
        do{
            
            resultMatches =  resultadoDataService.GetAllMatchs()
            
        }catch let erro as NSError{
            print("error al cargar resultados \(erro)")
        }
        
        return resultMatches
    }
    

    
    
}
