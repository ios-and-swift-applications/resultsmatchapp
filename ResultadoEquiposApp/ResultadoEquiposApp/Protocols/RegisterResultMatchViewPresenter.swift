//
//  RegisterResultMatchPresenter.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 12/6/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import CoreData

protocol RegisterResultMatchViewPresenter {
    
    init(view:RegisterResultMatchView,manageContext:NSManagedObjectContext)
    
    func RegisterResultMatch(partido:PartidoStruct)
    
    func GetAllResultMatches() -> [PartidoStruct]
}
