//
//  LoginViewPresenter.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import CoreData
protocol LoginViewPresenter {
    
    
    init(view:LoginView,_manageContex:NSManagedObjectContext)
    func LoginUsuario(email:String,contrasena:String) -> UsuarioStruct

}
