//
//  LoginView.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import UIKit

protocol LoginView {
    
    func mostrarAlertaLoginExitoso()
    func mostrarAlertaLoginFallido()
    func pasarAPantallaDeListadoResultados()
    
}
