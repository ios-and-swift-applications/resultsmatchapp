//
//  RegisterResultMatchViewController.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 12/6/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit


class RegisterResultMatchViewController: UIViewController {

    @IBOutlet weak var txtNameLocalTeam: UITextField!
    @IBOutlet weak var txtNameVisitTeam: UITextField!
    @IBOutlet weak var txtResultLocalTeam: UITextField!
    
    @IBOutlet weak var txtResultVisitTeam: UITextField!
    
    @IBOutlet weak var txtCompetition: UITextField!
    
    @IBOutlet weak var DtPDateMatch: UIDatePicker!
    
    private var registerMatchResultPresente : RegisterResultMatchViewPresenter!
    private var coreDataStack:CoreDataStack!
    var newResultMatch:PartidoStruct!
    var listaResults : [PartidoStruct]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coreDataStack = CoreDataStack(dataModelName: "ResultadoEquiposApp")
        self.registerMatchResultPresente = RegisterResultMatchPresenter(view: self, manageContext: self.coreDataStack.contextObject)
        
        listaResults = self.registerMatchResultPresente.GetAllResultMatches()
        
        print("lista resultados \(listaResults!)")
        
        }
    

    @IBAction func btnRegisterResult(_ sender: UIButton) {
        
        let date : NSDate = DtPDateMatch.date as NSDate
        
        newResultMatch  = PartidoStruct(Competicion:txtCompetition.text!,NameLocalTeam:txtNameLocalTeam.text!,
                                        NameVisitTeam:txtNameVisitTeam.text!, MarcadorLocal:txtResultLocalTeam.text!,
                                        MarcadorVisitante:txtResultVisitTeam.text!,FechaPartido:date)
        do{
            try self.registerMatchResultPresente.RegisterResultMatch(partido: newResultMatch)
            ShowAlertRegisterSuccess()
            
        }catch let error as NSError{
            print("Error")
        }
        
    }


}

extension RegisterResultMatchViewController : RegisterResultMatchView{
    
    func ShowAlertRegisterSuccess() {
        let alertSuccess = UIAlertController(title: "guardado", message: "Resultdo guardado", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertSuccess.addAction(okAction)
        
        present(alertSuccess,animated: true)
    }
    
    
}
