//
//  RegisterViewController.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit
import  CoreData

class RegisterViewController: UIViewController, RegisterView {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    
    var manageContext : NSManagedObjectContext!
    var registerPresenter : RegisterPresenter!
    var coreDataStack : CoreDataStack!
    
    var newUser:UsuarioStruct!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.coreDataStack =  CoreDataStack(dataModelName: "ResultadoEquiposApp")
        self.manageContext = coreDataStack.contextObject
        registerPresenter = RegisterPresenter(view: self, manageContext: self.manageContext)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnRegisterUser(_ sender: Any) {
        newUser = UsuarioStruct(Name:txtName.text!,Phone:txtPhone.text!,
                                Email:txtEmail.text!,Password:"1234")
        
        print("usuario \(newUser!)")
        registerPresenter.registrarUsuario(usuario: newUser)
    }
    
    func mostrarAlertaRegistroCorrecto() {
        
        let sucessAlert = UIAlertController(title: "Registro correcto",
                                            message: "El usuario se ha registrado con exito", preferredStyle: .alert)
        
        let okAcction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        sucessAlert.addAction(okAcction)
        present(sucessAlert,animated: true)
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


