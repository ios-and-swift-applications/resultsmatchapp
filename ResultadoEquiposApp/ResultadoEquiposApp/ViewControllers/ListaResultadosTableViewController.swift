//
//  ListaResultadosTableViewController.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class ListaResultadosTableViewController: UITableViewController {
    
    private var registreResultPresenter : RegisterResultMatchPresenter!
    var listaMatchResults: [PartidoStruct]!
    private var coredataStack : CoreDataStack!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coredataStack = CoreDataStack(dataModelName: "ResultadoEquiposApp")
        self.registreResultPresenter = RegisterResultMatchPresenter(view: self, manageContext: self.coredataStack.contextObject)
        self.GetAllMatchsResult()
        
    }
    
    
    func GetAllMatchsResult() -> Void {
        
        self.listaMatchResults  = registreResultPresenter.GetAllResultMatches()
        
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.listaMatchResults.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultadoPartidoCell", for: indexPath) as! ResultadoPartidoTableViewCell
        cell.updateCell()
        
        cell.lbNameLocalTeam.text = listaMatchResults[indexPath.row].NameLocalTeam
        cell.lbNameVisitTeam.text = listaMatchResults[indexPath.row].NameVisitTeam
        
        cell.lbResultLocalTeam.text = listaMatchResults[indexPath.row].MarcadorLocal
        print("Valor \(listaMatchResults[indexPath.row].MarcadorLocal)")
        print("Valor 2 \(listaMatchResults[indexPath.row].MarcadorVisitante)")
        cell.lbResultTeamVisit.text = listaMatchResults[indexPath.row].MarcadorVisitante

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListaResultadosTableViewController : RegisterResultMatchView{
    func ShowAlertRegisterSuccess() {
        print("Tablax")
    }
    
    
}
