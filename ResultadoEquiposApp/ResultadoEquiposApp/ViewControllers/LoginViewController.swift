//
//  LoginViewController.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit
import CoreData


class LoginViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    private var manageContext:NSManagedObjectContext!
    private var loginPresenter : LoginPresenter!
    private var coreDataStack : CoreDataStack!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coreDataStack = CoreDataStack(dataModelName: "ResultadoEquiposApp")
        self.manageContext = self.coreDataStack.contextObject
        loginPresenter = LoginPresenter(view: self,_manageContex: self.manageContext)
        
        txtPassword.text = "1234"
        txtEmail.text = "c@gmail.com"
        
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        
        guard let userName = txtEmail.text,
            let password = txtPassword.text else {
                return
        }
        do{
            var userFound = try loginPresenter.LoginUsuario(email: userName, contrasena: password)
            self.performSegue(withIdentifier: "ingresarSegue", sender: nil)
            //mostrarAlertaLoginExitoso()
            
        }catch let error as NSError{
            mostrarAlertaLoginFallido()
        }
    }

}

extension LoginViewController : LoginView{
    
    func mostrarAlertaLoginExitoso() {
        
        let sucessAlert = UIAlertController(title: "inicio correcto", message: "Usuario o contrasela incorrectos", preferredStyle: .actionSheet)
        
        let alertOkAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sucessAlert.addAction(alertOkAction)
        
        present(sucessAlert,animated: true)
        
        
    }
    
    func mostrarAlertaLoginFallido() {
        let errorAlert = UIAlertController(title: "Error al ingresar sesion", message: "Usuario o contraseña incorrecto", preferredStyle: .actionSheet)
        let alerOkAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        errorAlert.addAction(alerOkAction)
        present(errorAlert,animated: true)
    }
    
    func pasarAPantallaDeListadoResultados() {
        print("hola")
    }
    
    
}
