//
//  Partido.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

struct PartidoStruct {
    var Competicion:String
    var NameLocalTeam:String
    var NameVisitTeam:String
    var MarcadorLocal:String
    var MarcadorVisitante:String
    var FechaPartido:NSDate
}
