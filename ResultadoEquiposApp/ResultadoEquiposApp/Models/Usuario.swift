//
//  User.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

struct UsuarioStruct {
    var Name:String
    var Phone:String
    var Email:String
    var Password : String
    
}
