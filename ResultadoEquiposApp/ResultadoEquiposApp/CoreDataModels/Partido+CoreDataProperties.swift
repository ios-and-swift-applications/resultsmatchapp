//
//  Partido+CoreDataProperties.swift
//  
//
//  Created by Jorge luis Menco Jaraba on 12/8/18.
//
//

import Foundation
import CoreData


extension Partido {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Partido> {
        return NSFetchRequest<Partido>(entityName: "Partido")
    }

    @NSManaged public var competition: String?
    @NSManaged public var macthDate: NSDate?
    @NSManaged public var resultLocalTeam: String?
    @NSManaged public var resultVisitTeam: String?
    @NSManaged public var nameLocalTeam: String?
    @NSManaged public var nameVisitTeam: String?

}
