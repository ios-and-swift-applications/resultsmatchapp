//
//  Usuario+CoreDataProperties.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/26/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//
//

import Foundation
import CoreData


extension Usuario {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Usuario> {
        return NSFetchRequest<Usuario>(entityName: "Usuario")
    }

    @NSManaged public var email: String?
    @NSManaged public var name: String?
    @NSManaged public var phone: String?
    @NSManaged public var password: String?

}
