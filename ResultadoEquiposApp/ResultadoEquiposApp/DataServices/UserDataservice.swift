//
//  UserDataservice.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import CoreData

class UserDataService {
    
    private var _manageContext: NSManagedObjectContext
    
    init(manageContext:NSManagedObjectContext) {
        self._manageContext = manageContext
    }
    
    func Login(email:String,password:String) -> UsuarioStruct {
        var usuarioFound: UsuarioStruct!
        let predicateUsername = NSPredicate(format: "email == %@", NSString(string: email))
        let predicatePassword = NSPredicate(format: "password == %@", NSString(string: password))
        
        let predicadoAnd = NSCompoundPredicate(type: .and, subpredicates: [predicateUsername,predicatePassword])
        
        let loginFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        loginFetchRequest.predicate = predicadoAnd
        
        do{
            let resultado = try _manageContext.fetch(loginFetchRequest)
            let usuarioResult = resultado.first! as? Usuario
            
            let nameUser = usuarioResult?.name
            let phoneUser = usuarioResult?.phone
            let emailUser = usuarioResult?.email
            let passwordUser = usuarioResult?.password
            
            usuarioFound = UsuarioStruct(Name:nameUser!,Phone:phoneUser!,Email:emailUser!,Password:passwordUser!)
            
            print("usuario nombre \(usuarioFound!)")

        }catch let error as NSError{
            print("hubo un error \(error)")
        }
        return usuarioFound
    }
    
    func RegisterUser(user:UsuarioStruct) -> Void {
     
        let nuevoUsuario = Usuario(context: self._manageContext)
        
        do {
            nuevoUsuario.email = user.Email
            nuevoUsuario.name = user.Name
            nuevoUsuario.password = user.Password
            nuevoUsuario.phone = user.Phone
            
            try self._manageContext.save()
            
        }catch let error as NSError {
            print("ocurrio un error al guardar el usuario \(error)")
        }
    }
    
}
