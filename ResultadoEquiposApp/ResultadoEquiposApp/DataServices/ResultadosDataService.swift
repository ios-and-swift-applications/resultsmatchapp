//
//  ResultadosDataService.swift
//  ResultadoEquiposApp
//
//  Created by Jorge luis Menco Jaraba on 11/28/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import CoreData


class ResultadosDataService{
    
    private var manageContext: NSManagedObjectContext
    
    
    init(_manageContext:NSManagedObjectContext) {
        self.manageContext = _manageContext
        
    }
    
    
    func SaveResultMatch(partido:PartidoStruct) -> Void {
        
        // se crea una instacia de la entidad partido de core data
    
        let resultMatch  = Partido(context: self.manageContext)
        
        resultMatch.competition = partido.Competicion
        resultMatch.resultLocalTeam = partido.MarcadorLocal
        resultMatch.resultVisitTeam = partido.MarcadorVisitante
        resultMatch.nameLocalTeam = partido.NameLocalTeam
        resultMatch.nameVisitTeam = partido.NameVisitTeam
        resultMatch.macthDate = NSDate()
        // se manda a gaurdar los datos a core data
        do{
            
            try self.manageContext.save()
            
        }catch let error as NSError{
            print("ocurrio un error al guardar el partido \(error)")
        }
        
    }
    
    func GetAllMatchs() -> [PartidoStruct] {
        var resultMathces : [PartidoStruct] = []
        var nombreEquipoLocalAux : String = ""
        var nombreEquipoVisitaAux : String = ""
        let getAllResultRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Partido")
        do{
            
            let resultMatchs = try self.manageContext.fetch(getAllResultRequest)

            var resultMatch : PartidoStruct!
            for item in resultMatchs as! [Partido] {
                
                guard let competicion = item.competition,
                    let marcadorLocal = item.resultLocalTeam,
                    let marcadorvisita = item.resultVisitTeam,
                    let fecha = item.macthDate else{
                        return resultMathces
                }
                
                if var nombreEquipoLocal = item.nameLocalTeam {
                    nombreEquipoLocalAux = item.nameLocalTeam!
                }else{
                    nombreEquipoLocalAux = ""
                }
               if var nombreEquipoVisita = item.nameVisitTeam {

                    nombreEquipoVisitaAux = item.nameVisitTeam!
               }else{
                    nombreEquipoVisitaAux  = ""
                }
                
                resultMatch = PartidoStruct(Competicion:competicion, NameLocalTeam:nombreEquipoLocalAux,NameVisitTeam:nombreEquipoVisitaAux,MarcadorLocal:marcadorLocal,MarcadorVisitante:marcadorvisita,FechaPartido:fecha)
                resultMathces.append(resultMatch)
            }
            
        }catch let error as NSError{
            print("error \(error)")
        }
        return resultMathces
    }
    
    
    
    
    
    
}
